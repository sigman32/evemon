﻿namespace EVEMon.LogitechG15
{
    public enum LcdState
    {
        SplashScreen = 0,
        CharacterList = 1,
        SkillComplete = 2,
        Character = 3,
        CycleSettings = 4,
        Refreshing = 5
    }
}